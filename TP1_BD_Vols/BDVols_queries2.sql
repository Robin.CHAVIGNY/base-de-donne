-- 28-Quels sont les n° des vols au départ de Nice et dont l’heure d’arrivée est inférieure à 18h ?
SELECT VOLNUM FROM VOL WHERE VILLEDEP='NICE' AND HEUREARR < '18:00';

-- 29-Afficher les noms des pilotes sous la forme : "MIRANDA S.".
SELECT PLNOM||' '||SUBSTR(PLPRENOM,1,1)||'.' PLNAME FROM PILOTE;
-- 30-Afficher la durée des vols.
SELECT HEUREARR - HEUREDEP FROM VOL;
-- 31-Trier la liste des vols par ville de départ puis ville d'arrivée.
SELECT * FROM VOL  ORDER BY VILLEDEP,VILLEARR;
-- 32-Quels sont les pilotes habitant la même ville et ayant le même salaire que MIRANDA ?
SELECT PLNOM FROM PILOTE WHERE VILLE = (SELECT VILLE FROM PILOTE WHERE PLNOM='MIRANDA') AND SALAIRE = (SELECT SALAIRE FROM PILOTE WHERE PLNOM = 'MIRANDA');
-- 33-Quel est le nombre de vols effectués par chaque pilote ?
SELECT COUNT(V.VOLNUM) FROM VOL V RIGHT JOIN PILOTE P ON P.PLNUM=V.PLNUM GROUP BY V.PLNUM;
-- 34-Trouver le nombre de vols par pilote et par avion.
SELECT COUNT(V.VOLNUM) FROM VOL V RIGHT JOIN PILOTE P ON P.PLNUM=V.PLNUM GROUP BY V.PLNUM,V.AVNUM; 
-- 35-Donner le nombre de vols par pilote s’il est supérieur à 2.
SELECT COUNT(V.VOLNUM) FROM VOL V RIGHT JOIN PILOTE P ON P.PLNUM=V.PLNUM GROUP BY V.PLNUM HAVING COUNT(V.VOLNUM)>2;
-- 36-Donner le nom des pilotes effectuant au moins 2 vols.

/!/

SELECT P.PLNOM P1 FROM PILOTE P JOIN VOL V ON V.PLNUM=P.PLNUM WHERE (SELECT COUNT(V.VOLNUM) FROM VOL WHERE V.PLNUM=(SELECT PLNUM FROM PILOTE WHERE PLNOM=P1))>1;
-- 37-Quels sont les numéros des pilotes qui conduisent tous les avions de la compagnie ?

-- 38-Quels sont les avions localisés dans la même ville que l’avion n° 3 ?

-- 39-Lister les paires de numéros de pilote (paires inverses et identiques supprimées).

-- 40-Donner les pilotes (num, nom et prénom) dont le nombre de vols est supérieur ou égal à 5.

-- 41-Donner les numéros des pilotes pilotant tous les avions de type A310 ?

-- 42-Donner le nom des pilotes pilotant tous les avions localisés à Lyon ?

-- 43-Donner le numéro et le nom des avions autres que ceux de type Boeing,
-- qui sont conduits par tous les pilotes ayant le plus gros salaire.
